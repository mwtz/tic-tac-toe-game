package game;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class Main {

    private static final ArrayList<Integer> PLAYER_POSITIONS = new ArrayList<>();
    private static final ArrayList<Integer> CPU_POSITIONS = new ArrayList<>();

    public static void main(String[] args) {
        char[][] gameBoard = {
                {' ', '|', ' ', '|', ' '},
                {'-', '+', '-', '+', '-'},
                {' ', '|', ' ', '|', ' '},
                {'-', '+', '-', '+', '-'},
                {' ', '|', ' ', '|', ' '},
        };

        int playerPosition;
        int cpuPosition;
        Random random = new Random();
        Scanner scanner;

        System.out.println("Welcome! \nLet's play Tic Tac Toe!");
        printGameBoard(gameBoard);
        System.out.println();

        while (true) {
            scanner = new Scanner(System.in);
            System.out.println("Please enter your placement (1-9): ");
            playerPosition = scanner.nextInt();


            while (playerPosition < 1 || playerPosition > 9) {
                System.out.println("Invalid number! Valid placements are 1 - 9. \nPlease enter your placement (1-9): ");
                playerPosition = scanner.nextInt();
            }
            // Keep player from overwriting his own previous positions and also the cpu's positions
            while (PLAYER_POSITIONS.contains(playerPosition) || CPU_POSITIONS.contains(playerPosition)) {
                System.out.println("Position already taken. Enter a valid position: ");
                playerPosition = scanner.nextInt();
            }

            System.out.println("You entered placement: " + playerPosition);
            placeSymbol(gameBoard, "player", playerPosition);
            printGameBoard(gameBoard);
            /*
            If player has won or if the board is now full (tie), the game is over,
            and we don't want the cpu to have another turn.
            */
            if (checkWinOrTie()) {
                break;
            }

            /*
             Generate cpuPosition, but keep cpu from overwriting its own
             previous positions as well as the player's positions
            */
            do {
                // Values range from 1 to 9
                cpuPosition = random.nextInt(9) + 1;
            } while (PLAYER_POSITIONS.contains(cpuPosition) || CPU_POSITIONS.contains(cpuPosition));

            System.out.println("The computer enters placement: " + cpuPosition);
            placeSymbol(gameBoard,"cpu", cpuPosition);
            printGameBoard(gameBoard);

            if (checkWinOrTie()) {
                break;
            }
        }
        scanner.close();
    }

    public static void printGameBoard(char[][] gameBoard) {
        for (char[] row : gameBoard) {
            for (char symbol : row) {
                System.out.print(symbol);
            }
            System.out.println();
        }
    }

    // Place symbol on gameBoard for user
    public static void placeSymbol(char[][] gameBoard, String user, int position) {
        char symbol = ' ';
        if (user.equals("player")) {
            symbol = 'X';
            // Keep track of user's positions
            PLAYER_POSITIONS.add(position);
        } else if (user.equals("cpu")) {
            symbol = 'O';
            // Keep track of cpu's positions
            CPU_POSITIONS.add(position);
        }
        // Store the symbol in the designated position on the game-board
        switch (position) {
            case 1:
                gameBoard[0][0] = symbol;
                break;
            case 2:
                gameBoard[0][2] = symbol;
                break;
            case 3:
                gameBoard[0][4] = symbol;
                break;
            case 4:
                gameBoard[2][0] = symbol;
                break;
            case 5:
                gameBoard[2][2] = symbol;
                break;
            case 6:
                gameBoard[2][4] = symbol;
                break;
            case 7:
                gameBoard[4][0] = symbol;
                break;
            case 8:
                gameBoard[4][2] = symbol;
                break;
            case 9:
                gameBoard[4][4] = symbol;
                break;
            default:
                System.out.println("Invalid position");
                break;
        }
    }

    // Do the PLAYER_POSITIONS or CPU_POSITIONS that have been played
    // contain a winning sequence of positions? If they don't, is
    // there a tie?
    // TODO:
    // Introduce String or Player typed parameter 'player' so that
    // the method will only check for one particular player whether
    // the positions that he has played contain a winning sequence
    public static boolean checkWinOrTie() {
        String result = "";
        boolean winOrTie = false;

        // Winning sequences of positions
        List<Integer> topRow = Arrays.asList(1, 2, 3);
        List<Integer> middleRow = Arrays.asList(4, 5, 6);
        List<Integer> bottomRow = Arrays.asList(7, 8, 9);
        List<Integer> leftColumn = Arrays.asList(1, 4, 7);
        List<Integer> middleColumn = Arrays.asList(2, 5, 8);
        List<Integer> rightColumn = Arrays.asList(3, 6, 9);
        List<Integer> diagonal1 = Arrays.asList(1, 5, 9);
        List<Integer> diagonal2 = Arrays.asList(7, 5, 3);

        // A list of all winning sequences
        List<List<Integer>> winningSequences = new ArrayList<>();
        winningSequences.add(topRow);
        winningSequences.add(middleRow);
        winningSequences.add(bottomRow);
        winningSequences.add(leftColumn);
        winningSequences.add(middleColumn);
        winningSequences.add(rightColumn);
        winningSequences.add(diagonal1);
        winningSequences.add(diagonal2);

        // For each list of winning combinations in the list of allWinning,
        // check whether playerPositions or cpuPositions contain a winning condition
        for (List<Integer> winningSequence : winningSequences) {
            if (PLAYER_POSITIONS.containsAll(winningSequence)) {
                winOrTie = true;
                result = "\nYou won! :-)";
            } else if (CPU_POSITIONS.containsAll(winningSequence))  {
                    winOrTie = true;
                    result = "\nThe computer won! :-(";
            }
            // It's (only) a tie if no-one has won AND the board is full
            if (!winOrTie && ((PLAYER_POSITIONS.size() + CPU_POSITIONS.size()) == 9)) {
                winOrTie = true;
                result = "\nSnap, it's a tie!";
            }
        }
        System.out.println(result);
        return winOrTie;
    }
}
